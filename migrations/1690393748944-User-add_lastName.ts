import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserAddLastName1690393748944 implements MigrationInterface {
  name = 'UserAddLastName1690393748944';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`user\` ADD \`lastName\` varchar(255) NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE \`post_entity\` DROP PRIMARY KEY`);
    await queryRunner.query(`ALTER TABLE \`post_entity\` DROP COLUMN \`id\``);
    await queryRunner.query(
      `ALTER TABLE \`post_entity\` ADD \`id\` int NOT NULL PRIMARY KEY`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE \`post_entity\` DROP COLUMN \`id\``);
    await queryRunner.query(
      `ALTER TABLE \`post_entity\` ADD \`id\` int NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`post_entity\` ADD PRIMARY KEY (\`id\`)`,
    );
    await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`lastName\``);
  }
}
