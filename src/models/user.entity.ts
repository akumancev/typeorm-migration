import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  // @Column({ default: true })
  // isActive: boolean;

  getId(): number {
    return this.id;
  }

  getFirstName(): string {
    return this.firstName;
  }
  setFirstName(firstName: string) {
    this.firstName = firstName;
  }
}
